package com.unitedofoq.otmsCustomization.controller;

import com.unitedofoq.otmsCustomization.entity.CustomOuser;
import com.unitedofoq.otmsCustomization.service.CustomOuserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomOuserController {

    @Autowired
    CustomOuserService customOuserService;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<Void> createUser() {

        CustomOuser user = new CustomOuser();
        user.setLoginName("Islam");
        System.out.println("Creating User " + user.getLoginName());

        if (customOuserService.SaveOuser(user)) {
            System.out.println("A User with name " + user.getLoginName() + " saved");
            return new ResponseEntity<Void>(HttpStatus.ACCEPTED);
        }
        return new ResponseEntity<>(HttpStatus.METHOD_FAILURE);
    }
}
