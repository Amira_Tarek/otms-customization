package com.unitedofoq.otmsCustomization.service;

import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.otmsCustomization.entity.CustomOuser;
import com.unitedofoq.otmsCustomization.repository.CustomOuserRepository;
import com.unitedofoq.otmsCustomization.utilities.userConverter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomOuserService {

    @Autowired
    CustomOuserRepository customOuserRepository;
    @Autowired
    userConverter userConverter;

    private OEntityManagerRemote oEntityManagerRemote;

    public Boolean SaveOuser(CustomOuser customOuser) {

        ejbLookup();

        if (customOuser != null) {
            customOuserRepository.save(customOuser);
            OUser oUser = userConverter.trimCustomOuser(customOuser);
            try {

                System.out.println(oEntityManagerRemote);
                oEntityManagerRemote.saveEntity(oUser, oUser);
            } catch (Exception e) {
                return false;
            }
            return true;
        }
        return false;
    }

    public void ejbLookup() {

        InitialContext initialContext;
        try {
            initialContext = new InitialContext();
            oEntityManagerRemote = (OEntityManagerRemote) initialContext.lookup("java:global/ofoq/com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote");

        } catch (NamingException ex) {
            Logger.getLogger(CustomOuserService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
