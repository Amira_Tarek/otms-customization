package com.unitedofoq.otmsCustomization.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "customOuser")
@Entity
public class CustomOuser {

    /*
    * 1[DBID]
      ,2[UTYPE]
      ,3[EMAIL]
      ,4[RESERVED]
      ,5[BIRTHDATE]
      ,6[LASTMAINTDTIME]
      ,7[PASSWORD]
      ,[CUSTOM2]
      ,[ACTIVE]
      ,[LOGINNAME]
      ,[DELETED]
      ,[CUSTOM3]
      ,[CUSTOM1]
      ,[CC2_DBID]
      ,[PARTICIPANTROLES_DBID]
      ,[CC5_DBID]
      ,[CC1_DBID]
      ,[CC3_DBID]
      ,[FIRSTLANGUAGE_DBID]
      ,[CC4_DBID]
      ,[LASTMAINTUSER_DBID]
      ,[ENTITYSTATUS_DBID]
      ,[MANAGERROLEENABLED]
      ,[REQUESTNOTE]
      ,[RTL]
      ,[LDAP]
      ,[OMODULE_DBID]
      ,[SECONDLANGUAGE_DBID]
      ,[USERPREFRENCE1]
      ,[USERPREFRENCE2]
      ,[LANGUAGE_DBID]
      ,[userPrefrence3]
      ,[userPrefrence5]
      ,[userPrefrence7]
      ,[userPrefrence9]
      ,[userPrefrence4]
      ,[userPrefrence6]
      ,[userPrefrence8]
      ,[userPrefrence10]
      ,[timezone_dbid]
      ,[TRACKMETADATA]
      ,[TRACKBUSINESSDATA]
      ,[displayName]
      ,[GROUP1_DBID]
      ,[GROUP2_DBID]
      ,[passwordEnc]*/
    @Id
    @GeneratedValue
    Long DBID;
    String utype;
    String email;
    int reserved;
    Date birthday, lastMaintainedTime;
    String password;
    String custom1, custom2, custom3;
    int active;
    String loginName;
    int deleted;
    Long CC1_DBID, CC2_DBID, CC3_DBID, CC4_DBID, CC5_DBID;
    Long participantRoles_DBID;
    Long firstLanguage_DBID, secondLanguage_DBID;
    Long lastMaintUser_DBID;
    Long entityStatus_DBID;
    int managerRoleEnabled;
    String requestedNote;
    int RTL;
    int LDAP;
    Long omodule_DBID;
    int userPrefrence1, userPrefrence2, userPrefrence3,
            userPrefrence4, userPrefrence5, userPrefrence6, userPrefrence7,
            userPrefrence8, userPrefrence9, userPrefrence10;
    Long langauge_DBID;
    Long timezone_DBID;
    int trackMetaData;
    int trackBusinessData;
    String displayName;
    Long group1_DBID, group2_DBID;
    String passwordEnc;
//    my new data in entity

    String newField1;
    String newField2;

    public Long getDBID() {
        return DBID;
    }

    public void setDBID(Long DBID) {
        this.DBID = DBID;
    }

    public String getUtype() {
        return utype;
    }

    public void setUtype(String utype) {
        this.utype = utype;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getReserved() {
        return reserved;
    }

    public void setReserved(int reserved) {
        this.reserved = reserved;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Date getLastMaintainedTime() {
        return lastMaintainedTime;
    }

    public void setLastMaintainedTime(Date lastMaintainedTime) {
        this.lastMaintainedTime = lastMaintainedTime;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCustom1() {
        return custom1;
    }

    public void setCustom1(String custom1) {
        this.custom1 = custom1;
    }

    public String getCustom2() {
        return custom2;
    }

    public void setCustom2(String custom2) {
        this.custom2 = custom2;
    }

    public String getCustom3() {
        return custom3;
    }

    public void setCustom3(String custom3) {
        this.custom3 = custom3;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public Long getCC1_DBID() {
        return CC1_DBID;
    }

    public void setCC1_DBID(Long CC1_DBID) {
        this.CC1_DBID = CC1_DBID;
    }

    public Long getCC2_DBID() {
        return CC2_DBID;
    }

    public void setCC2_DBID(Long CC2_DBID) {
        this.CC2_DBID = CC2_DBID;
    }

    public Long getCC3_DBID() {
        return CC3_DBID;
    }

    public void setCC3_DBID(Long CC3_DBID) {
        this.CC3_DBID = CC3_DBID;
    }

    public Long getCC4_DBID() {
        return CC4_DBID;
    }

    public void setCC4_DBID(Long CC4_DBID) {
        this.CC4_DBID = CC4_DBID;
    }

    public Long getCC5_DBID() {
        return CC5_DBID;
    }

    public void setCC5_DBID(Long CC5_DBID) {
        this.CC5_DBID = CC5_DBID;
    }

    public Long getParticipantRoles_DBID() {
        return participantRoles_DBID;
    }

    public void setParticipantRoles_DBID(Long participantRoles_DBID) {
        this.participantRoles_DBID = participantRoles_DBID;
    }

    public Long getFirstLanguage_DBID() {
        return firstLanguage_DBID;
    }

    public void setFirstLanguage_DBID(Long firstLanguage_DBID) {
        this.firstLanguage_DBID = firstLanguage_DBID;
    }

    public Long getSecondLanguage_DBID() {
        return secondLanguage_DBID;
    }

    public void setSecondLanguage_DBID(Long secondLanguage_DBID) {
        this.secondLanguage_DBID = secondLanguage_DBID;
    }

    public Long getLastMaintUser_DBID() {
        return lastMaintUser_DBID;
    }

    public void setLastMaintUser_DBID(Long lastMaintUser_DBID) {
        this.lastMaintUser_DBID = lastMaintUser_DBID;
    }

    public Long getEntityStatus_DBID() {
        return entityStatus_DBID;
    }

    public void setEntityStatus_DBID(Long entityStatus_DBID) {
        this.entityStatus_DBID = entityStatus_DBID;
    }

    public int getManagerRoleEnabled() {
        return managerRoleEnabled;
    }

    public void setManagerRoleEnabled(int managerRoleEnabled) {
        this.managerRoleEnabled = managerRoleEnabled;
    }

    public String getRequestedNote() {
        return requestedNote;
    }

    public void setRequestedNote(String requestedNote) {
        this.requestedNote = requestedNote;
    }

    public int getRTL() {
        return RTL;
    }

    public void setRTL(int RTL) {
        this.RTL = RTL;
    }

    public int getLDAP() {
        return LDAP;
    }

    public void setLDAP(int LDAP) {
        this.LDAP = LDAP;
    }

    public Long getOmodule_DBID() {
        return omodule_DBID;
    }

    public void setOmodule_DBID(Long omodule_DBID) {
        this.omodule_DBID = omodule_DBID;
    }

    public int getUserPrefrence1() {
        return userPrefrence1;
    }

    public void setUserPrefrence1(int userPrefrence1) {
        this.userPrefrence1 = userPrefrence1;
    }

    public int getUserPrefrence2() {
        return userPrefrence2;
    }

    public void setUserPrefrence2(int userPrefrence2) {
        this.userPrefrence2 = userPrefrence2;
    }

    public int getUserPrefrence3() {
        return userPrefrence3;
    }

    public void setUserPrefrence3(int userPrefrence3) {
        this.userPrefrence3 = userPrefrence3;
    }

    public int getUserPrefrence4() {
        return userPrefrence4;
    }

    public void setUserPrefrence4(int userPrefrence4) {
        this.userPrefrence4 = userPrefrence4;
    }

    public int getUserPrefrence5() {
        return userPrefrence5;
    }

    public void setUserPrefrence5(int userPrefrence5) {
        this.userPrefrence5 = userPrefrence5;
    }

    public int getUserPrefrence6() {
        return userPrefrence6;
    }

    public void setUserPrefrence6(int userPrefrence6) {
        this.userPrefrence6 = userPrefrence6;
    }

    public int getUserPrefrence7() {
        return userPrefrence7;
    }

    public void setUserPrefrence7(int userPrefrence7) {
        this.userPrefrence7 = userPrefrence7;
    }

    public int getUserPrefrence8() {
        return userPrefrence8;
    }

    public void setUserPrefrence8(int userPrefrence8) {
        this.userPrefrence8 = userPrefrence8;
    }

    public int getUserPrefrence9() {
        return userPrefrence9;
    }

    public void setUserPrefrence9(int userPrefrence9) {
        this.userPrefrence9 = userPrefrence9;
    }

    public int getUserPrefrence10() {
        return userPrefrence10;
    }

    public void setUserPrefrence10(int userPrefrence10) {
        this.userPrefrence10 = userPrefrence10;
    }

    public Long getLangauge_DBID() {
        return langauge_DBID;
    }

    public void setLangauge_DBID(Long langauge_DBID) {
        this.langauge_DBID = langauge_DBID;
    }

    public Long getTimezone_DBID() {
        return timezone_DBID;
    }

    public void setTimezone_DBID(Long timezone_DBID) {
        this.timezone_DBID = timezone_DBID;
    }

    public int getTrackMetaData() {
        return trackMetaData;
    }

    public void setTrackMetaData(int trackMetaData) {
        this.trackMetaData = trackMetaData;
    }

    public int getTrackBusinessData() {
        return trackBusinessData;
    }

    public void setTrackBusinessData(int trackBusinessData) {
        this.trackBusinessData = trackBusinessData;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Long getGroup1_DBID() {
        return group1_DBID;
    }

    public void setGroup1_DBID(Long group1_DBID) {
        this.group1_DBID = group1_DBID;
    }

    public Long getGroup2_DBID() {
        return group2_DBID;
    }

    public void setGroup2_DBID(Long group2_DBID) {
        this.group2_DBID = group2_DBID;
    }

    public String getPasswordEnc() {
        return passwordEnc;
    }

    public void setPasswordEnc(String passwordEnc) {
        this.passwordEnc = passwordEnc;
    }

    public String getNewField1() {
        return newField1;
    }

    public void setNewField1(String newField1) {
        this.newField1 = newField1;
    }

    public String getNewField2() {
        return newField2;
    }

    public void setNewField2(String newField2) {
        this.newField2 = newField2;
    }
}
