package com.unitedofoq.otmsCustomization.dto;

import com.unitedofoq.fabs.core.security.user.OUser;

import java.util.Date;

public class oldOuserDto extends OUser {

    Long DBID;
    String utype;
    String email;
    int reserved;
    Date birthday, lastMaintainedTime;
    String password;
    String custom1, custom2, custom3;
    int active;
    String loginName;
    int deleted;
    Long CC1_DBID, CC2_DBID, CC3_DBID, CC4_DBID, CC5_DBID;
    Long participantRoles_DBID;
    Long firstLanguage_DBID, secondLanguage_DBID;
    Long lastMaintUser_DBID;
    Long entityStatus_DBID;
    int managerRoleEnabled;
    String requestedNote;
    int RTL;
    int LDAP;
    Long omodule_DBID;
    int userPrefrence1, userPrefrence2, userPrefrence3,
            userPrefrence4, userPrefrence5, userPrefrence6, userPrefrence7,
            userPrefrence8, userPrefrence9, userPrefrence10;
    Long langauge_DBID;
    Long timezone_DBID;
    int trackMetaData;
    int trackBusinessData;
    String displayName;
    Long group1_DBID, group2_DBID;
    String passwordEnc;
}
