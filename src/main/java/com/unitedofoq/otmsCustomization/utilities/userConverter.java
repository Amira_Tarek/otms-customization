package com.unitedofoq.otmsCustomization.utilities;

import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.otmsCustomization.dto.oldOuserDto;
import com.unitedofoq.otmsCustomization.entity.CustomOuser;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class userConverter {

    public OUser trimCustomOuser(CustomOuser customOuser) {
        ModelMapper modelMapper = new ModelMapper();
        oldOuserDto oldOuserDto = modelMapper.map(customOuser, oldOuserDto.class);
        return oldOuserDto;
    }

//    @Bean
//    public Context getIntialContext() throws NamingException {
//        Properties jndiProps = new Properties();
//        jndiProps.put("java.naming.factory.initial",
//                "org.jboss.naming.remote.client.InitialContextFactory");
//        jndiProps.put("jboss.naming.client.ejb.context", true);
//        jndiProps.put("java.naming.provider.url",
//                "http-remoting://localhost:18181");
//        return new InitialContext(jndiProps);
//    }
//    @EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote",
//        beanInterface = OEntityManagerRemote.class)
}
