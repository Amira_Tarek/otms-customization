package com.unitedofoq.otmsCustomization.repository;

import com.unitedofoq.otmsCustomization.entity.CustomOuser;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface CustomOuserRepository extends JpaRepository<CustomOuser, Long> {

}
